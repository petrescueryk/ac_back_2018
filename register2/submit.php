<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$sex="";
$facultate="";
$error=0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

//creez sex
if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
//creez facultate
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}
//casute goale
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($check) || empty($captcha_inserted ) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}
//valid nume, prenume
if(strlen($firstname) < 3 || strlen($firstname)>20 || strlen($lastname) < 3 || strlen($lastname)>20){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}
//valid intrebare
if(strlen($question)<15){
	$error=1;
	$error_text="RIP";
}
//validare telefon
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}
//validare mail
for($i=0;$i<strlen($email);$i++){
	$arond=0;
	$pct=0;
	if($email[$i]=='@') $arond++;
	if($email[$i]=='.' && $arond>0) $pct++;
	if($i==strlen($email) && ($arond!=1 || $pct!=1) ){
		$error=1;
		$error_text="Email gresit";
	}
}
//verif cnp
if(strlen($cnp)<13 || !is_numeric($cnp) || $cnp[0]==7 || $cnp[0]==8 || $cnp[0]==9 || $cnp[0]==0){
	$error=1;
	$error_text="CNP gresit";
}
//validare varsta, am considerat data actuala ca fiind 01.01.2019 si toate datele trebuie sa fie pana in 2001 
if($birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]>2001 || $birth[0]*1000+$birth[1]*100+$birth[2]*10+$birth[3]<=1919){
	$error=1;
	$error_text="Varsta nu este acceptata";
}
//validare captcha
if(strlen($captcha_generated)==strlen($captcha_inserted)){
	for($i=0;$i<=strlen($captcha_inserted);$i++){
		if($captcha_generated[$i]!=$captcha_inserted[$i])
			$error=1;
			$error_text="Wrong captcha";
			break;
	}
}
//camp facultate
if(strlen($facultate)<3 || strlen($facultate)>30){
		$error=1;
		$error_text="Facultate incorecta";
}
//analizam data de nastere si CNP
if($cnp[1]!=$birth[2] || $cnp[2]!=$birth[3] || $cnp[3]!=$birth[5] || $cnp[4]!=$birth[6] || $cnp[5]!=$birth[8] || $cnp[6]!=$birth[9] ){
	$error=1;
	$error_text="Data de nastere si CNP incompatibile";
}

// verif link facebook https://www.facebook.com/
$link="https://www.facebook.com/";
for($i=1;$i<strlen($link);$i++){
	if($link[$i]!=$facebook[$i]){
		$error=1;
		$error_text="Facebook incorect";
	}

}
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
//verificare nedublare email, google was my friend 
$pdo1="SELECT * FROM register2 where email='$email'";
$pdoresult1=$con->query($pdo1);
$pdorcount1=$pdoresult1->rowCount();
if($pdorcount1>=1)
{$error=1;
$error_text="Email-ul este invalid!!!";
}
//50 participanti 
$query2 = "SELECT * FROM register2";
$result2 = $con-> query($query2);
$rowcount2 = $result2 -> rowCount();
if($rowcount2>=50){
	$error=1;
	$error_text="Sunt prea multi participanti";
}


if($error==0){

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:facultate,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
//adaug in baza de date ceea ce e scris in facultate
$stmt2 -> bindParam(':facultate',$facultate);
//testam sexul
if($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5){
	$sex='M';
	$stmt2 -> bindParam(':sex',$sex);
}
else {
	$sex='F';
	$stmt2 -> bindParam(':sex',$sex);
}

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";


}else{
    echo "Succes";
}
}
else {
	echo $error_text;
}
?>
