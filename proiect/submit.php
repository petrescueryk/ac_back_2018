<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$facebook = "";
$question = "";
$facultate="";
$varsta=0;
$error=0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['varsta'])){
	$varsta = $_POST['varsta'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}


if(isset($_POST['question'])){
	$question = $_POST['question'];
}

//creez facultate
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}
//casute goale
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($question) ||  empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}
//valid nume, prenume
if(strlen($firstname) < 3 || strlen($firstname)>20 || strlen($lastname) < 3 || strlen($lastname)>20){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}
//valid intrebare
if(strlen($question)>30){
	$error=1;
	$error_text="RIP";
}
if($varsta<18){
	$error=1;
	$error_text="RIP";
}
//validare telefon
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}
//validare mail
for($i=0;$i<strlen($email);$i++){
	$arond=0;
	$pct=0;
	if($email[$i]=='@') $arond++;
	if($email[$i]=='.') $pct++;
	if($i==strlen($email) && ($arond!=1 || $pct!=1) ){
		$error=1;
		$error_text="Email gresit";
	}
}
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
//verificare nedublare email, google was my friend
$query = $con -> prepare("SELECT 'email' FROM 'register2' WHERE 'email' = ?");
$query -> bindValue(1,$email);
$query -> execute();
if($query->rowCount() !=0 ){
	$error=1;
	$error_text="Email-ul exista deja";
}
if($error==0){

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,question,facultate) VALUES(:firstname,:lastname,:phone,:email,:question,:facultate)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':question',$question);
//adaug in baza de date ceea ce e scris in facultate
$stmt2 -> bindParam(':facultate',$facultate);
if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";


}else{
    echo "Succes";
}
}
else {
	echo $error_text;
}
?>
